Demo shows `/404.html` not to be displayed even though page exists.

See [issue #183](https://gitlab.com/gitlab-org/gitlab-pages/issues/183) for the latest details.